/*
 * brbr-phantom-crawler
 * 
 *
 * Copyright (c) 2015 Janne Aalto
 * Licensed under the MIT license.
 */

'use strict';


module.exports = function (grunt) {

    // Please see the Grunt documentation for more information regarding task
    // creation: http://gruntjs.com/creating-tasks
    var phantom = require('node-phantom'),
        URI = require('URIjs'),
        _ = require('lodash'),
        Q = require('q');

    grunt.registerMultiTask('brbr_phantom_crawler', 'crawl site urls', function () {

        // Merge task-specific and/or target-specific options with these defaults.
        var options = this.options({
            baseUrl: 'http://localhost',
            depth: 5,
            log: null,
            file: 'paths.js',
            fragment: false
        });

        var test = ["http://unitedbankers.fi/","/","http://filial.dev/#","#", "#tets","http://filial.dev/realtillgangar/","http://filial.dev/realtillgangar/#test","http://filial.dev/fonder/","http://filial.dev/strukturerade-investeringar/","http://filial.dev/formogenhetsforvaltning/","http://filial.dev/om-ub/","http://filial.dev/ub-kapitalforvaltning-ab/", "mailto:janne@barabra.fi"];


        var done = this.async(),
            urls = [],
            baseUrl = new URI(options.baseUrl),
            depth = options.depth + baseUrl.segment().length,
            log = options.log,
            file = options.file,
            fragment = options.fragment,
            document;

        var createObj = function(url){
            var uri = URI(url);
            return {
                url: uri.toString(),
                crawled: false,
                depth: uri.segment().length
            };
        };

        var absoluteUrl = function(url){
            var uri = URI(url);

            if(uri.is('relative')){
                uri = URI(baseUrl.toString() + url).normalizePathname();
            }

            if(!fragment){
                uri = uri.fragment("");
            }

            return uri.toString();
        };

        var validateUrl = function (url) {
            //uri.segment(); uri.fragment("");

            var ret = true,
                uri = URI(url);

            if(uri.suffix() == 'jpg' || uri.suffix() == 'png' || uri.suffix() == 'jpeg' || uri.suffix() == 'PNG' || uri.suffix() == 'JPG' || uri.suffix() == 'JPEG' || uri.suffix() == 'pdf' || uri.suffix() == 'PDF'){
                ret = false;
            }

            if(!fragment){
                uri = uri.fragment("");
            }

            if(uri.is('relative')){
                uri = URI(baseUrl.toString() + url).normalizePathname();
            }

            if(!uri.is('url')){
                ret = false;
            }

            if(baseUrl.normalizeProtocol().protocol() !== uri.normalizeProtocol().protocol() ||
                baseUrl.normalizeHostname().host() !== uri.normalizeHostname().host()){
                ret = false;
            }

            if(uri.length === 0){
                ret = false;
            }
            if(uri.segment().length > depth){
                ret = false;
            }

            if(ret){
                return uri.toString();
            }else{
                return false;
            }

        };

        //start crawling
        var crawl = function (url) {
            //console.log(url);
            var deferred = Q.defer();
            var page;
            page = phantom.create(function (err, ph) {
                ph.createPage(function (err, page) {
                    return page.open(url, function (err, status) {
                        setTimeout(function(){
                            return page.evaluate(function () {
                                return [].map.call(document.querySelectorAll('a'), function (link) {
                                    return link.getAttribute('href');
                                });
                            }, function (err, result) {
                                deferred.resolve(result);
                                ph.exit();
                            });
                        }, 1000);
                    });
                });

            }, {parameters:{'web-security':false}});
            return deferred.promise;
        };

        //iterate until every url is crawled through
        var iterateOver = function (urls) {

            var deferred = Q.defer();
            var url = _.result(_.find(urls, 'crawled', false), 'url');

            if(log){
                grunt.log.writeln('crawling ' + url);
            }

            if(typeof url !== undefined || url.length !== 0){

                crawl(url).then(function(data){

                    urls[_.findIndex(urls, {'url': url})].crawled = true;
                    return _.map(_.difference(_.uniq(_.map(_.filter(data, validateUrl), absoluteUrl)), _.map(urls, 'url')), createObj);

                }).then(function(objs){

                    urls = urls.concat(objs);
                    deferred.resolve(urls);

                });

            }

            return deferred.promise;

        };

        var onSuccess = function(urls){

            if (_.every(urls, 'crawled', true)) {

                grunt.file.write(file, JSON.stringify(_.map(urls, 'url')));

                if (log) {
                    // Print a success message.
                    grunt.log.writeln('File "' + file + '" created.');
                }

                done();

            }else{

                var left = _.pluck(_.filter(urls, 'crawled', false), 'url');

                if(log){
                    grunt.log.writeln(left);
                    grunt.log.writeln(urls.length-left.length + ' of ' + urls.length);

                }

                iterateOver(urls).then(onSuccess);

            }

        };


        if (baseUrl.is('url') && baseUrl.is('absolute')) {

            //initial value
            urls.push({
                url: baseUrl.toString(),
                crawled: false,
                depth: baseUrl.segment().length
            });

            if (log) {
                grunt.log.writeln('Starting crawl "' + urls[0].url + '" ...');
            }



            iterateOver(urls).then(onSuccess);

            //console.log(_.map(_.difference(_.uniq(_.map(_.filter(test, validateUrl), absoluteUrl)), _.map(urls, 'url')), createObj));


        } else {
            //not absolute or url -> fail
            grunt.log.writeln('use absolute path');
            return false;
        }

        // Write file with paths.



    });

};
